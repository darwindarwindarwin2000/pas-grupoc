package ar.edu.unlp.pas.tp.controllers;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import ar.edu.unlp.pas.tp.dto.ProductoDTO;
import ar.edu.unlp.pas.tp.model.Categoria;
import ar.edu.unlp.pas.tp.model.Direccion;
import ar.edu.unlp.pas.tp.model.Persona;
import ar.edu.unlp.pas.tp.model.Producto;
import ar.edu.unlp.pas.tp.resources.ProductoActivaRequest;
import ar.edu.unlp.pas.tp.resources.ProductoRequest;
import ar.edu.unlp.pas.tp.resources.ProductoStockRequest;
import ar.edu.unlp.pas.tp.services.CategoriaService;
import ar.edu.unlp.pas.tp.services.DireccionService;
import ar.edu.unlp.pas.tp.services.PersonaService;
import ar.edu.unlp.pas.tp.services.ProductoService;



@RestController
@RequestMapping("api/productos")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @Autowired
    private CategoriaService categoriaService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private DireccionService direccionService;



    @GetMapping("/all")
    @Secured({"ADMIN"})
  public List<ProductoDTO> getAllProductos(
     @RequestHeader("Authorization") String token
  ){
    return productoService.findAll();
  }


  @GetMapping("/busqueda")
  public List<ProductoDTO> busqueda(
    @RequestParam("keyword") String busqueda,
    @RequestParam("precioMin") float precioMin,
    @RequestParam("precioMax") float precioMax
  ){
    return productoService.findAll(busqueda,precioMin,precioMax);
  }

  @GetMapping("/publicaciones")
  @Secured({"USUARIO"})
  public List<ProductoDTO> getActiveProductos(
     @RequestHeader("Authorization") String token
  ){
    return productoService.findActive();
  }



  @PutMapping("/delete/{id}")
  @Secured("VENDEDOR")
  public ResponseEntity<?> delete(
    @RequestHeader("Authorization") String token,
    @PathVariable("id") Long id
  ){
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Long idVendedor = ((Persona) auth.getPrincipal()).getId();
    Optional<Producto> prod = productoService.findById(id);
    if(!prod.isPresent()){
      return ResponseEntity.badRequest().body("El producto no existe");
    }
    else if(prod.get().getVendedor().getId()!=idVendedor){
      return ResponseEntity.badRequest().body("No tiene permiso para eliminar dicho producto");
    }
    Producto producto = prod.get();
    producto.setDeleted(true);
    productoService.save(producto);
    return ResponseEntity.ok().body("Eliminacion exitosa");
  }


  @PostMapping("/create")
  @Secured("VENDEDOR")
  public ResponseEntity<?> create(
    @RequestHeader("Authorization") String token,
    @RequestBody ProductoRequest productoRequest
  ){
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Long idVendedor = ((Persona) auth.getPrincipal()).getId();
    Persona vendedor = personaService.findById(idVendedor).get();
    Optional<Categoria> optCategoria = categoriaService.findById(productoRequest.getIdCategoria());
    if(productoRequest.getPrecioMin()<=0 || productoRequest.getPrecioMax()<productoRequest.getPrecioMin() || productoRequest.getStock()<0){
      return ResponseEntity.badRequest().body("Error en el envio de parámetros");
    }
    else if(productoRequest.getPrecio()<productoRequest.getPrecioMin()||productoRequest.getPrecio()>productoRequest.getPrecioMax()){
      return ResponseEntity.badRequest().body("El precio debe estar entre el precio minimo y el precio maximo");
    }
    else if(vendedor.getDireccionesDeEnvio().stream().noneMatch(each ->each.getId()==productoRequest.getIdDireccion())&&!(vendedor.getDireccionDeFacturacion().getId()==productoRequest.getIdDireccion())){
      return ResponseEntity.badRequest().body("La dirección no existe o no pertenece al usuario");
    }
    else if(!optCategoria.isPresent()){
      return ResponseEntity.badRequest().body("La categoria es invalida");
    }
    else{

      Optional<Direccion> direccion = vendedor.getDireccionesDeEnvio().stream().filter(it->it.getId()==productoRequest.getIdDireccion()).findFirst();
      Producto producto = new Producto();
      producto.setNombre(productoRequest.getNombre());
      producto.setDescripcion(productoRequest.getDescripcion());
      producto.setPrecio(productoRequest.getPrecio());
      producto.setPrecioMax(productoRequest.getPrecioMax());
      producto.setPrecioMin(productoRequest.getPrecioMin());
      producto.setStock(productoRequest.getStock());
      if(producto.getStock()==0){
        producto.setActiva(false);
      }
      else{
        producto.setActiva(true);
      }
      producto.setCategoria(optCategoria.get());
      if(direccion.isPresent()){
      producto.setUbicacion(direccion.get());
      }
      else{
        producto.setUbicacion(vendedor.getDireccionDeFacturacion());
      }
      producto.setVendedor(vendedor);
      productoService.save(producto);
      return ResponseEntity.ok().body("Creacion exitosa");
    }
    
  }


  @PutMapping("/edit/{id}")
  @Secured("VENDEDOR")
  public ResponseEntity<?> create(
    @RequestHeader("Authorization") String token,
    @PathVariable("id") Long id,  
    @RequestBody ProductoRequest productoRequest
  ){
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Long idVendedor = ((Persona) auth.getPrincipal()).getId();
    Optional<Producto> prod = productoService.findById(id);
    Optional<Categoria> optCategoria = categoriaService.findById(productoRequest.getIdCategoria());
    if(!prod.isPresent()){
      return ResponseEntity.badRequest().body("El producto no existe");
    }
    else if(prod.get().getVendedor().getId()!=idVendedor){
      return ResponseEntity.badRequest().body("No tiene permiso para editar dicho producto");
    }
    else if(productoRequest.getPrecioMin()<=0 || productoRequest.getPrecioMax()<productoRequest.getPrecioMin() || productoRequest.getStock()<0){
      return ResponseEntity.badRequest().body("Error en el envio de parámetros");
    }
    else if(productoRequest.getPrecio()<productoRequest.getPrecioMin()||productoRequest.getPrecio()>productoRequest.getPrecioMax()){
      return ResponseEntity.badRequest().body("El precio debe estar entre el precio minimo y el precio maximo");
    }
    else if(prod.get().getVendedor().getDireccionesDeEnvio().stream().noneMatch(each ->each.getId()==productoRequest.getIdDireccion())&&!(prod.get().getVendedor().getDireccionDeFacturacion().getId()==productoRequest.getIdDireccion())){
      return ResponseEntity.badRequest().body("La dirección no existe o no pertenece al usuario");
    }
    else if(!optCategoria.isPresent()){
      return ResponseEntity.badRequest().body("La categoria es invalida");
    }
    else{
      Producto producto = prod.get();
      Optional<Direccion> direccion = producto.getVendedor().getDireccionesDeEnvio().stream().filter(it->it.getId()==productoRequest.getIdDireccion()).findFirst();
      producto.setNombre(productoRequest.getNombre());
      producto.setDescripcion(productoRequest.getDescripcion());
      producto.setPrecio(productoRequest.getPrecio());
      producto.setPrecioMax(productoRequest.getPrecioMax());
      producto.setPrecioMin(productoRequest.getPrecioMin());
      producto.setStock(productoRequest.getStock());
      if(producto.getStock()==0){
        producto.setActiva(false);
      }
      else{
        producto.setActiva(producto.isActiva());
      }
      producto.setCategoria(optCategoria.get());
      if(direccion.isPresent()){
      producto.setUbicacion(direccion.get());
      }
      else{
        producto.setUbicacion(producto.getVendedor().getDireccionDeFacturacion());
      }
      productoService.save(producto);
      return ResponseEntity.ok().body("Edicion exitosa");
    }
    
  }



  @PutMapping("/stock/{id}")
  @Secured("VENDEDOR")
  public ResponseEntity<?> updateStock(
    @RequestHeader("Authorization") String token,
    @PathVariable("id") Long id,  
    @RequestBody ProductoStockRequest productoRequest){
    Optional<Producto> producto = productoService.findById(id);
    if(producto.isPresent()){
      Producto prod = producto.get();
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      String email = auth.getName();
      if(email.equals(prod.getVendedor().getEmail())){
          prod.setStock(productoRequest.getStock());
          if(productoRequest.getStock()<0){
            return ResponseEntity.badRequest().body("El stock no puede ser menor a 0");
          }
          else if(productoRequest.getStock()==0){
            prod.setActiva(false);
          }
        productoService.save(prod);
        return ResponseEntity.ok(prod);
      }
      else{
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
        .body("No tienes permiso para cambiarle el stock a dicho producto");
      }
    }
    else{
      return ResponseEntity.badRequest().body("Producto no existe.");
    }
  }
  
  
  
  @PutMapping("/publicar/{id}")
  @Secured("VENDEDOR")
  public ResponseEntity updateStock(
    @RequestHeader("Authorization") String token,
    @PathVariable("id") Long id,    
    @RequestBody ProductoActivaRequest productoRequest){
    Optional<Producto> producto = productoService.findById(id);
    if(producto.isPresent()){
      Producto prod = producto.get();
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      String email = auth.getName();
      if(email.equals(prod.getVendedor().getEmail())){
        if(prod.getStock()==0&&productoRequest.isActiva()){
          return ResponseEntity.status(HttpStatus.FORBIDDEN)
            .body("El producto no tiene stock");
        }
        else{
          prod.setActiva(productoRequest.isActiva());
          productoService.save(prod);
          return ResponseEntity.ok().body("Actualizacion exitosa.");
        }
      }
      else{
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
        .body("No tienes permiso para cambiar el estado de la publicación");
      }
    }
    else{
      return ResponseEntity.badRequest().body("Producto no existe.");
    }
  }


}

