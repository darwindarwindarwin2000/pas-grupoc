package ar.edu.unlp.pas.tp.services;

import ar.edu.unlp.pas.tp.mappers.PersonaMapper;
import ar.edu.unlp.pas.tp.dto.PersonaDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.tp.model.*;
import ar.edu.unlp.pas.tp.repositories.IPersonaRepository;

@Service
public class PersonaService{

    @Autowired
    private IPersonaRepository personaRepository;

    @Autowired
    private PersonaMapper personaMapper;


    public List<PersonaDTO> findAll(){
        List<Persona> listP = personaRepository.findAll();
        List<PersonaDTO> listaDTO = new ArrayList<PersonaDTO>();
        for(Persona p:listP){
            PersonaDTO dto = personaMapper.personaToPersonaDTO(p);
            listaDTO.add(dto);
        }
        return listaDTO;
    }

    public Optional<Persona> findById(Long id){
        return personaRepository.findById(id);
    }

    public Persona save(Persona persona) {
        return personaRepository.save(persona);
    }

    public void remove(Persona persona){
        personaRepository.delete(persona);
    }


    public Optional<Persona> findByEmail(String email) { /** EL PROFE LO HABIA COMENTADO PERO NO SE POR QUE**/
        List<Persona> listP = personaRepository.findAll();
        return listP.stream().filter(
          (Persona persona) -> persona.getEmail().equals(email)
        ).findFirst();
    }
}
