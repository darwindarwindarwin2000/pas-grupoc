package ar.edu.unlp.pas.tp.resources.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class AuthRequest {
  @JsonProperty("email")
  @NotNull @Email @Length(min = 5, max = 320)
  @Getter @Setter private String email;

  @JsonProperty("password")
  @NotNull @Length(min = 5, max = 60)
  @Getter @Setter private String password;
}
