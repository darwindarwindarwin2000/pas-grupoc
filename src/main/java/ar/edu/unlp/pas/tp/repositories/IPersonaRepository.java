package ar.edu.unlp.pas.tp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.pas.tp.model.*;
import java.util.Optional;

@Repository
public interface IPersonaRepository extends JpaRepository<Persona,Long>{
    public Optional<Persona> findByEmail(String email);
}
