package ar.edu.unlp.pas.tp.mappers;

import ar.edu.unlp.pas.tp.dto.PersonaDTO;
import java.util.*;

import org.mapstruct.*;

import ar.edu.unlp.pas.tp.model.*;
import ar.edu.unlp.pas.tp.services.PersonaService;

@Mapper(componentModel="spring", uses=PersonaService.class)
public interface PersonaMapper {

    @Mapping(source="id",target="id")
    @Mapping(target = "nombreCompleto",expression= "java(persona.getNombres()+' '+persona.getApellido())")
    @Mapping(target = "edad",expression = "java(calculateAge(persona.getFechaNacimiento()))")
    @Mapping(target="roles", expression="java(persona.getRoles().toString())")
    PersonaDTO personaToPersonaDTO(Persona persona);


    default int calculateAge(Date dateOfBirth) {
        if (dateOfBirth == null) {
            return 0;
        }
        Calendar dob = Calendar.getInstance();
        dob.setTime(dateOfBirth);
        Calendar now = Calendar.getInstance();
        int age = now.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (now.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }
}
