package ar.edu.unlp.pas.tp.resources.security;

import java.security.Key;
import java.util.Date;

import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ar.edu.unlp.pas.tp.model.Persona;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenUtil {
  private static final long EXPIRE_DURATION = 24 * 60 * 60 * 1000; // 24 hour

  @Value("${spring.datasource.secretKey}")
  private String SECRET_KEY;

  private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenUtil.class);

  public String generateAccessToken(Persona persona) {
      byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
      Key key = Keys.hmacShaKeyFor(keyBytes);

    return Jwts.builder()
      .setSubject(String.format("%s,%s", persona.getId(), persona.getEmail()))
      .setIssuer("CodeJava")
      .claim("roles", persona.getRoles().toString())
      .setIssuedAt(new Date())
      .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION))
      .signWith(key)
      .compact();
  }

  public boolean validateAccessToken(String token) {
    try {
        Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token);
        return true;
    } catch (ExpiredJwtException ex) {
        LOGGER.error("JWT expired. " + ex.getMessage());
    } catch (IllegalArgumentException ex) {
        LOGGER.error("Token is null, empty or only whitespace. " + ex.getMessage());
    } catch (MalformedJwtException ex) {
        LOGGER.error("JWT is invalid", ex);
    } catch (UnsupportedJwtException ex) {
        LOGGER.error("JWT is not supported", ex);
    } catch (SecurityException ex) {
        LOGGER.error("Signature validation failed");
    }

    return false; // false;
  }

  public String getSubject(String token) {
    return parseClaims(token).getSubject();
  }

  public Claims parseClaims(String token) {
    return Jwts.parser()
      .setSigningKey(SECRET_KEY)
      .parseClaimsJws(token)
      .getBody();
  }
}
