package ar.edu.unlp.pas.tp.services;

import ar.edu.unlp.pas.tp.repositories.RolRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.tp.model.*;

@Service
public class RolService{

  @Autowired
  private RolRepository rolRepository;

  public Rol save(Rol rol) {
    return rolRepository.save(rol);
  }

  public Rol findByName(String nombre) {
    List<Rol> listR = rolRepository.findAll();
    Optional<Rol> optionalRol = listR.stream().filter(
      (Rol rol) -> rol.getNombre().equals(nombre)
    ).findFirst();
    return optionalRol.orElse(null);
  }
}
