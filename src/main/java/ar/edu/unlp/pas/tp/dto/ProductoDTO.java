package ar.edu.unlp.pas.tp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ar.edu.unlp.pas.tp.model.Direccion;
import ar.edu.unlp.pas.tp.model.Persona;
import lombok.Getter;
import lombok.Setter;

public class ProductoDTO {
    

    @JsonProperty("id")
    @Getter @Setter private Long id;

    @JsonProperty("Nombre producto")
    @Getter @Setter private String nombre;

    @JsonProperty("Precio")
    @Getter @Setter private float precio;

    @JsonProperty("Precio minimo")
    @Getter @Setter private float precioMin;

    @JsonProperty("Precio maximo")
    @Getter @Setter private float precioMax;

    @JsonProperty("Stock")
    @Getter @Setter private int stock;

    @JsonProperty("Descripcion")
    @Getter @Setter private String descripcion;

    @JsonProperty("Ubicación")
    @Getter @Setter private Direccion ubicacion;

    @JsonProperty("Nombre completo")
    @Getter @Setter private String nombreCompleto;

    @JsonProperty("Email")
    @Getter @Setter private String email;

    @JsonProperty("Direccion de facturacion")
    @Getter @Setter private Direccion direccion;
 
}
