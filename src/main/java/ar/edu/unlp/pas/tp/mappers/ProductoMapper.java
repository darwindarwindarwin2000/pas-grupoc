package ar.edu.unlp.pas.tp.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import ar.edu.unlp.pas.tp.dto.ProductoDTO;
import ar.edu.unlp.pas.tp.model.Producto;
import ar.edu.unlp.pas.tp.services.ProductoService;

@Mapper(componentModel="spring", uses=ProductoService.class)
public interface ProductoMapper {


  @Mapping(source="id",target="id")
  @Mapping(target = "nombreCompleto",expression= "java(producto.getVendedor().getNombres()+' '+producto.getVendedor().getApellido())")
  @Mapping(source = "nombre", target="nombre")
  @Mapping(source = "precio",target = "precio")
  @Mapping(source = "precioMin",target = "precioMin")
  @Mapping(source = "precioMax",target = "precioMax")
  @Mapping(source = "stock",target = "stock")
  @Mapping(source = "descripcion",target = "descripcion")
  @Mapping(source = "ubicacion",target = "ubicacion")  
  @Mapping(target = "email" ,expression= "java(producto.getVendedor().getEmail())")
  @Mapping(target = "direccion",expression= "java(producto.getVendedor().getDireccionDeFacturacion())")
  ProductoDTO productoToProductoDTO(Producto producto);
}
