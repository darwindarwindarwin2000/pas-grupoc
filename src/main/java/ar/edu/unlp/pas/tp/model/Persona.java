package ar.edu.unlp.pas.tp.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

import java.io.Serializable;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import lombok.Data;

import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Entity
@Table(name="Persona")
@Data
public class Persona implements UserDetails,Serializable{

  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE)
  private Long id;

  @NotNull(message = "Apellido no puede ser nulo")
  @Column(name="apellido", length=50, nullable=false, unique=false)
  private String apellido;

  @NotNull(message = "Nombres no puede ser nulo")
  @Column(name="nombres", length=50, nullable=false, unique=false)
  private String nombres;

  @PastOrPresent(message = "Fecha no valida")
  @Temporal(TemporalType.DATE)
  private Date fechaNacimiento;
  public void setFechaNacimiento(Date newFechaNacimiento) {
      fechaNacimiento = newFechaNacimiento;
      age = ChronoUnit.YEARS.between(
        convertToLocalDateViaInstant(newFechaNacimiento),
        LocalDate.now()
      );
  }

  @Min(value = 18, message = "La edad deberia de ser mayor a 18 años")
  @Max(value = 125, message = "La edad deberia ser menor a 125 años")
  private long age;

  @NotNull(message = "Email no puede ser nulo")
  @Email(message = "Email no valido")
  @Column(name="email", length=320, nullable=false, unique=true)
  private String email;
  public String getUsername() {return email;}

  @NotNull(message = "La contraseña no puede ser nula.")
  @Column(name = "password", length = 64, nullable = false, unique = false)
  private String password;

  @OneToOne
  @NotNull(message = "Se necesita una direccion de facturacion")
  private Direccion direccionDeFacturacion = null;


  @ManyToMany
  @NotEmpty
  @NotNull
  private Set<Direccion> direccionesDeEnvio = new HashSet<Direccion>();

  public void addDireccionDeEnvio ( Direccion dir ) {
      direccionesDeEnvio.add(dir);
  }

  public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
    return dateToConvert.toInstant()
      .atZone(ZoneId.systemDefault())
      .toLocalDate();
  }

  @Override
  public String toString() {
    return id + " - " + nombres + " - " + apellido + " - " + email + " - "
      + password;
  }
  
  @ManyToMany
  @JoinTable(
    name = "roles_persona",
    joinColumns = @JoinColumn(name = "id_persona"),
    inverseJoinColumns = @JoinColumn(name = "id_rol")
  )
  private Set<Rol> roles = new HashSet<>();
  public void agregarRol(Rol nuevoRol) {
    roles.add(nuevoRol);
  }
  public void quitarRol(Rol rol) {
    roles.remove(rol);
  }

  @Override public boolean isEnabled() {return true;}
  @Override public boolean isCredentialsNonExpired() {return true;}
  @Override public boolean isAccountNonLocked() {return true;}
  @Override public boolean isAccountNonExpired() {return true;}
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    for (Rol rol : roles) {
        authorities.add(new SimpleGrantedAuthority(rol.getNombre()));
    }
    return authorities;
  }


}
