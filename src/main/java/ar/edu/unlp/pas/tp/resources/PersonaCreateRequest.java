package ar.edu.unlp.pas.tp.resources;

import com.fasterxml.jackson.annotation.JsonProperty;
import ar.edu.unlp.pas.tp.model.Persona;
import ar.edu.unlp.pas.tp.dto.DireccionDTO;
import ar.edu.unlp.pas.tp.model.Direccion;
import ar.edu.unlp.pas.tp.model.Rol;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
import java.util.Set;

public class PersonaCreateRequest {

    @JsonProperty("Nombre")
    @Getter @Setter private String nombre;

    @JsonProperty("Apellido")
    @Getter @Setter private String apellido;

    @JsonProperty("Contraseña")
    @Getter @Setter private String contraseña;

    @JsonProperty("Fecha de nacimineto")
    @Getter @Setter private String fechaNacimiento;

    @JsonProperty("Email")
    @Getter @Setter private String email;

    @JsonProperty("Roles")
    @Getter @Setter private Set<String> roles;

    @JsonProperty("Direccion de facturacion")
    @Getter @Setter private DireccionDTO direccionDeFacturacion;

    @JsonProperty("Direcciones de envio")
    @Getter @Setter private Set<DireccionDTO> direccionesDeEnvio;

}
