package ar.edu.unlp.pas.tp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ar.edu.unlp.pas.tp.model.Persona;

import ar.edu.unlp.pas.tp.model.Direccion;
import ar.edu.unlp.pas.tp.model.Rol;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

public class PersonaDTO{

    @JsonProperty("id") /////*** ESTO LO DEBERIAMOS DE ELIMINAR */
    @Getter @Setter private Long id;

    @JsonProperty("Nombre completo")
    @Getter @Setter private String nombreCompleto;

    @JsonProperty("Nombre de usuario")
    @Getter @Setter private String username;

    @JsonProperty("Edad")
    @Getter @Setter private int edad;

    @JsonProperty("Email")
    @Getter @Setter private String email;

    @JsonProperty("Roles")
    @Getter @Setter private String roles;

    @JsonProperty("Direccion de facturacion")
    @Getter @Setter private Direccion direccionDeFacturacion;

    @JsonProperty("Direcciones de envio")
    @Getter @Setter private Set<Direccion> direccionesDeEnvio;

}
