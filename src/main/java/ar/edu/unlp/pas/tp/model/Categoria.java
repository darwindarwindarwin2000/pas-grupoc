package ar.edu.unlp.pas.tp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name="Categoria")
@Data
public class Categoria {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Long id;

    
    @NotNull(message="El nombre de la categoría no puede ser nulo")
    private String nombre;
    
}
