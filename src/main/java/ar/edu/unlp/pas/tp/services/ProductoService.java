package ar.edu.unlp.pas.tp.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.tp.dto.ProductoDTO;
import ar.edu.unlp.pas.tp.mappers.ProductoMapper;
import ar.edu.unlp.pas.tp.model.Producto;
import ar.edu.unlp.pas.tp.repositories.IProductoRepository;

@Service
public class ProductoService {

  @Autowired
  private IProductoRepository productoRepository;

  @Autowired
  private ProductoMapper productoMapper;

  public List<ProductoDTO> findAll(){
    return productoRepository.findAll().stream().filter(each-> !each.isDeleted()).map(each ->productoMapper.productoToProductoDTO(each)).collect(Collectors.toList());
  }


  public List<ProductoDTO> findAll(String busqueda,float precioMin,float precioMax){
    return productoRepository.buscarProductos(busqueda, precioMin, precioMax).stream().filter(each-> !each.isDeleted()).map(each ->productoMapper.productoToProductoDTO(each)).collect(Collectors.toList());
  }

  public List<ProductoDTO> findActive(){
    return productoRepository.findAll().stream().filter(each->!each.isDeleted()).filter(each->each.isActiva()).map(each ->productoMapper.productoToProductoDTO(each)).collect(Collectors.toList());
    
  }
  public Optional<Producto> findById(long id){
    return productoRepository.findById(id).filter(each->!each.isDeleted());
  }

  public Producto save(Producto producto){
    return productoRepository.save(producto);
  }

  public void remove(Producto producto){
    productoRepository.delete(producto);
  }

  


  
}
