package ar.edu.unlp.pas.tp.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.NotNull;
import lombok.*;

public class ProductoStockRequest {  
    @JsonProperty("stock")
    @NotNull 
    @Getter @Setter private int stock;

}