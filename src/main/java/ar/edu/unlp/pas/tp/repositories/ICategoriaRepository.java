package ar.edu.unlp.pas.tp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.pas.tp.model.Categoria;

@Repository
public interface ICategoriaRepository extends JpaRepository<Categoria,Long>{

}
