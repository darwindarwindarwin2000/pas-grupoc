package ar.edu.unlp.pas.tp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.pas.tp.model.*;

@Repository
public interface RolRepository extends JpaRepository<Rol,Long> {

}
