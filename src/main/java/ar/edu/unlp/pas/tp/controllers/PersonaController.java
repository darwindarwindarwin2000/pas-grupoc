package ar.edu.unlp.pas.tp.controllers;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;

import ar.edu.unlp.pas.tp.ApplicationSecurity;
import ar.edu.unlp.pas.tp.dto.DireccionDTO;
import ar.edu.unlp.pas.tp.dto.PersonaDTO;
import ar.edu.unlp.pas.tp.mappers.DireccionMapper;
import ar.edu.unlp.pas.tp.services.DireccionService;
import ar.edu.unlp.pas.tp.services.PersonaService;
import ar.edu.unlp.pas.tp.services.RolService;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ar.edu.unlp.pas.tp.model.*;

import org.aspectj.weaver.patterns.PerObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

import ar.edu.unlp.pas.tp.model.Persona;
import ar.edu.unlp.pas.tp.resources.PersonaCreateRequest;

@RestController
@RequestMapping("api/personas")
public class PersonaController{

  @Autowired
  private PersonaService personaService;

  @Autowired
  private DireccionService direccionService;

  @Autowired
  private RolService rolService;

  @Autowired
  private DireccionMapper direccionMapper;

  @GetMapping("/all")
  @Secured({"ADMIN", "USUARIO"})
  public List<PersonaDTO> getAllPersonas(
          @RequestHeader("Authorization") String token
  ){
    return personaService.findAll();
  }

  @PostMapping("/create")
  @PreAuthorize("hasAuthority('ADMIN','USUARIO')")
  public ResponseEntity createPersona(
    @RequestHeader("Authorization") String token,
    @RequestBody PersonaCreateRequest personaCreateRequest
  ) throws ParseException{

    Direccion dirFact = 
    direccionService.save(personaCreateRequest.getDireccionDeFacturacion().toDireccion());

    Set<Direccion> direccionesEnv = personaCreateRequest.getDireccionesDeEnvio().stream().map(dirDTO -> dirDTO.toDireccion()).collect(Collectors.toSet());;
    direccionesEnv.forEach(dir -> direccionService.save(dir));

    Persona p = new Persona();
    p.setNombres(personaCreateRequest.getNombre());
    p.setApellido(personaCreateRequest.getApellido());
    p.setEmail(personaCreateRequest.getEmail());
    p.setFechaNacimiento(
      new SimpleDateFormat("dd/MM/yyyy").parse(personaCreateRequest.getFechaNacimiento())
    );
    PasswordEncoder passwordEncoder = (new ApplicationSecurity()).passwordEncoder();
    String password1 = passwordEncoder.encode(personaCreateRequest.getContraseña());
    p.setPassword(password1);
    p.setDireccionDeFacturacion(dirFact);
    p.getDireccionesDeEnvio().addAll(direccionesEnv);

    Set<String> rolesSt = personaCreateRequest.getRoles();
    rolesSt.forEach(rolSt -> {
      Rol rol = rolService.findByName(rolSt);
      if (rol != null) {
        p.agregarRol(rol);
      }
    });

    Persona savedPersona = personaService.save(p);

    return ResponseEntity.created(URI.create("/api/personas/" + savedPersona.getId()))
            .body(savedPersona);
  }

  @DeleteMapping("/delete/{unEmail}")
  public ResponseEntity deletePersonaByEmail(
          @RequestHeader("Authorization") String token,
          @PathVariable(value="unEmail") String email
          //@RequestParam("email") String email
  ) {

    Optional<Persona> p = personaService.findByEmail(email);

    p.ifPresent(persona -> personaService.remove(persona));

    if (p.isPresent()) {
      return ResponseEntity.ok().body("Eliminacion exitosa.");
    } else {
      return ResponseEntity.badRequest().body("Persona no existe.");
    }
  }
}
