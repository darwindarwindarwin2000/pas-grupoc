package ar.edu.unlp.pas.tp.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unlp.pas.tp.model.Persona;
import ar.edu.unlp.pas.tp.resources.security.AuthRequest;
import ar.edu.unlp.pas.tp.resources.security.AuthResponse;
import ar.edu.unlp.pas.tp.resources.security.JwtTokenUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class AuthController {

    @Autowired
    AuthenticationManager authManager;
    @Autowired
    JwtTokenUtil jwtUtil;
    
    @PostMapping("/api/login")
    public ResponseEntity<?> login(@RequestBody AuthRequest authRequest) {
        try {
            Authentication authentication = authManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authRequest.getEmail(), authRequest.getPassword()
                    )
            );
            Persona persona = (Persona) authentication.getPrincipal();
            String accessToken = jwtUtil.generateAccessToken(persona);
            AuthResponse authResponse = new AuthResponse(persona.getEmail(),accessToken);

            return ResponseEntity.ok(authResponse);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
