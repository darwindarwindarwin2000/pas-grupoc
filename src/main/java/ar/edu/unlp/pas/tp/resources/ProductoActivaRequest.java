package ar.edu.unlp.pas.tp.resources;


import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

public class ProductoActivaRequest {
    @JsonProperty("activa")
    @NotNull 
    @Getter @Setter private boolean activa;
  }
