package ar.edu.unlp.pas.tp.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.tp.model.*;
import ar.edu.unlp.pas.tp.dto.DireccionDTO;
import ar.edu.unlp.pas.tp.mappers.DireccionMapper;
import ar.edu.unlp.pas.tp.repositories.IDireccionRepository;

@Service
public class DireccionService {
  @Autowired
  private IDireccionRepository direccionRepository;

  @Autowired
  private DireccionMapper direccionMapper;

  public List<DireccionDTO> findAll() {
      List<Direccion> direcciones = direccionRepository.findAll();
      List<DireccionDTO> direccionesDTO = new ArrayList<DireccionDTO>();
      for(Direccion d:direcciones) {
          DireccionDTO ddto = direccionMapper.direccionToDireccionDTO(d);
          direccionesDTO.add(ddto);
      }
      return direccionesDTO;
  }

  public Direccion save(Direccion direccion) {
      return direccionRepository.save(direccion);
  }

  public void remove(Direccion direccion){
    direccionRepository.delete(direccion);
  }
}
