package ar.edu.unlp.pas.tp.mappers;

import ar.edu.unlp.pas.tp.dto.DireccionDTO;
import ar.edu.unlp.pas.tp.services.DireccionService;
import java.util.*;

import org.mapstruct.*;

import ar.edu.unlp.pas.tp.model.*;

@Mapper(componentModel="spring", uses=DireccionService.class)
public interface DireccionMapper {
  DireccionDTO direccionToDireccionDTO(Direccion direccion);
}
