package ar.edu.unlp.pas.tp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.pas.tp.model.Producto;

@Repository
public interface IProductoRepository extends JpaRepository<Producto,Long>,JpaSpecificationExecutor<Producto> {
    

    @Query("SELECT p FROM Producto p WHERE p.nombre LIKE %:busqueda% OR p.descripcion LIKE %:busqueda% AND p.precio >= :minPrice AND p.precio <= :maxPrice")
    public List<Producto> buscarProductos(String busqueda, float minPrice, float maxPrice);
}
