package ar.edu.unlp.pas.tp.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

public class ProductoRequest {

    @JsonProperty("nombre")
    @Getter @Setter private String nombre;
    
    @JsonProperty("precio")
    @Getter @Setter private float precio;

    @JsonProperty("stock")
    @Getter @Setter private int stock;

    @JsonProperty("precioMin")
    @Getter @Setter private float precioMin;

    @JsonProperty("precioMax")
    @Getter @Setter private float precioMax;

    @JsonProperty("descripcion")
    @Getter @Setter private String descripcion;

    @JsonProperty("activa")
    @Getter @Setter private boolean activa;

    @JsonProperty("ubicacion")
    @Getter @Setter private Long idDireccion;

    @JsonProperty("categoria")
    @Getter @Setter private Long idCategoria;

}
