package ar.edu.unlp.pas.tp.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unlp.pas.tp.model.Categoria;
import ar.edu.unlp.pas.tp.repositories.ICategoriaRepository;

@Service
public class CategoriaService{
    
    @Autowired
    private ICategoriaRepository categoriaRepository;
    
    public Optional<Categoria> findById(Long id){
        return categoriaRepository.findById(id);
    }
}
