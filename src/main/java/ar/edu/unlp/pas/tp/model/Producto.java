package ar.edu.unlp.pas.tp.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Data;


@Entity
@Table(name="Producto")
@Data
public class Producto implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Long id;


    @NotNull(message="El nombre del producto no puede ser nulo")
    private String nombre;

    @NotNull(message="El precio del producto no puede ser nulo")
    private float precio;

    private int stock=0;

    @NotNull(message ="El precio minimo no puede ser nulo")
    private float precioMin;

    @NotNull(message = "El precio maximo no puede ser nulo")
    private float precioMax;
    
    private String descripcion;

    private boolean activa = true;
    
    private boolean deleted = false;
    

    
    @OneToOne
    @NotNull(message="La ubicación del producto no puede ser nula")
    private Direccion ubicacion;

    @ManyToOne
    @NotNull(message = "La categoria del producto no puede ser nula")
    private Categoria categoria;
    
    
    @ManyToOne
    @JoinColumn(name="persona_id")
    @NotNull(message = "El vendedor no puede ser nulo")
    private Persona vendedor;



}