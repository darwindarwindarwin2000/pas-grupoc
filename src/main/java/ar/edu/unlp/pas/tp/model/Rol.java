package ar.edu.unlp.pas.tp.model;

import javax.persistence.*;

import jakarta.validation.constraints.*;

import lombok.Getter;
import lombok.Setter;

// import java.util.Collections;
// import java.util.HashSet;
// import java.util.Set;

@Entity
@Table(name="Rol")
public class Rol {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Getter @Setter private Long id;

  @NotNull(message="El rol debe tener un nombre")
  @Column(name="nombre", length=50, nullable=false, unique=true)
  @Getter @Setter private String nombre;

  @Override
  public String toString() {
    return nombre;
  }

  // @NotEmpty
  // @OneToMany(cascade=CascadeType.ALL)
  // @JoinColumn(name="urls", referencedColumnName="id", nullable=false)
  // @Getter @Setter private Set<String> roles = new HashSet<Rol>();
}
