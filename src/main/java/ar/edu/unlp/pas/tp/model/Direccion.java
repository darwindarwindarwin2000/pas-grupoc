package ar.edu.unlp.pas.tp.model;

import javax.persistence.*;

import ar.edu.unlp.pas.tp.dto.DireccionDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="Direccion")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Direccion implements Serializable{
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Long id;

    @NotNull(message = "Calle no puede ser nula")
    @Column(name="calle", length=100, nullable=false, unique=false)
    private String calle;

    @NotNull(message = "Numero no puede ser nulo")
    @Column(name="numero", length=20, nullable=false, unique=false)
    private String numero;

    @Column(name="piso", length=20, nullable=true, unique=false)
    private String piso;

    @Column(name="departamento", length=20, nullable=true, unique=false)
    private String departamento;

    @NotNull(message = "Localidad no puede ser nula")
    @Column(name="localidad", length=100, nullable=false, unique=false)
    private String localidad;

    @NotNull(message = "Provincia no puede ser nula")
    @Column(name="provincia", length=50, nullable=false, unique=false)
    private String provincia;

    @NotNull(message = "Pais no puede ser nulo")
    @Column(name="pais", length=50, nullable=false, unique=false)
    private String pais;

    public Direccion(DireccionDTO dir){
        this.calle = dir.getCalle();
        this.departamento = dir.getDepartamento();
        this.localidad = dir.getLocalidad();
        this.pais = dir.getPais();
        this.provincia = dir.getProvincia();
        this.piso = dir.getPiso();
        this.numero = dir.getNumero();
    }
}
