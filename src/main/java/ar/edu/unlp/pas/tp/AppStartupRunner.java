package ar.edu.unlp.pas.tp;

import ar.edu.unlp.pas.tp.services.RolService;
import ar.edu.unlp.pas.tp.services.PersonaService;
import ar.edu.unlp.pas.tp.services.ProductoService;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import ar.edu.unlp.pas.tp.model.*;
import ar.edu.unlp.pas.tp.repositories.ICategoriaRepository;
import ar.edu.unlp.pas.tp.repositories.IDireccionRepository;
import ar.edu.unlp.pas.tp.services.DireccionService;

@Component
public class AppStartupRunner implements ApplicationRunner {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private RolService rolService;
    
    @Autowired
    private DireccionService direccionService;

    @Autowired
    private ProductoService productoService;
    
    @Autowired
    private IDireccionRepository direccionRepository;

    @Autowired ICategoriaRepository categoriaRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
      Rol rol1 = new Rol();
      rol1.setNombre("ADMIN");
      rolService.save(rol1);

      Rol rol2 = new Rol();
      rol2.setNombre("USUARIO");
      rolService.save(rol2);

     /* Rol rol3 = new Rol();
      rol3.setNombre("VENDEDOR");
      rolService.save(rol3);
      */
      Direccion direccion1 = new Direccion();
      direccion1.setCalle("15");
      direccion1.setNumero("981");
      direccion1.setPiso("2");
      direccion1.setDepartamento("3");
      direccion1.setLocalidad("La Plata");
      direccion1.setProvincia("Buenos Aires");
      direccion1.setPais("Argentina");
      direccionService.save(direccion1);
      
      Direccion direccion2 = new Direccion();
      direccion2.setCalle("125");
      direccion2.setNumero("9181");
      direccion2.setPiso("22");
      direccion2.setDepartamento("31");
      direccion2.setLocalidad("La Plata");
      direccion2.setProvincia("Buenos Aires");
      direccion2.setPais("Argentina");
      direccionService.save(direccion2);
      
      Direccion direccion3 = new Direccion();
      direccion3.setCalle("44");
      direccion3.setNumero("1981");
      direccion3.setPiso("10");
      direccion3.setDepartamento("10");
      direccion3.setLocalidad("CABA");
      direccion3.setProvincia("CABA");
      direccion3.setPais("Argentina");
      direccionService.save(direccion3);

     
      Persona persona1 = new Persona();
      persona1.setNombres("Juan");
      persona1.setApellido("Pérez");
      persona1.setEmail("a@gmail.com");
      persona1.setFechaNacimiento(
        new SimpleDateFormat("dd/MM/yyyy").parse("15/01/2000")
      );
      PasswordEncoder passwordEncoder = (new ApplicationSecurity()).passwordEncoder();
      String password1 = passwordEncoder.encode("a@gmail.com");
      persona1.setPassword(password1);
      persona1.setDireccionDeFacturacion(direccion1);
      persona1.getDireccionesDeEnvio().add(direccion1);
      persona1.getDireccionesDeEnvio().add(direccion3);
      persona1.agregarRol(rol1);
      //persona1.agregarRol(rol3);
      personaService.save(persona1);
      
      
      Persona persona2 = new Persona();
      persona2.setNombres("Juan");
      persona2.setApellido("Pérez");
      persona2.setEmail("b@gmail.com");
      persona2.setFechaNacimiento(
        new SimpleDateFormat("dd/MM/yyyy").parse("15/01/2000")
      );
      PasswordEncoder passwordEncoder2 = (new ApplicationSecurity()).passwordEncoder();
      String password2 = passwordEncoder2.encode("b@gmail.com");
      persona2.setPassword(password2);
      persona2.setDireccionDeFacturacion(direccion2);
      persona2.getDireccionesDeEnvio().add(direccion2);
      persona2.getDireccionesDeEnvio().add(direccion3);
      persona2.agregarRol(rol2);
      personaService.save(persona2);



      Categoria categoria = new Categoria();
      categoria.setNombre("Varios");
      categoriaRepository.save(categoria);

      

      Producto producto1 = new Producto();
      producto1.setNombre("prod1");
      producto1.setPrecio(15);
      producto1.setDescripcion("asd");
      producto1.setCategoria(categoria);
      producto1.setPrecioMax(100);
      producto1.setPrecioMin(10);
      producto1.setVendedor(persona1);
      producto1.setUbicacion(direccion3);
      productoService.save(producto1);
      


    }

}
