package ar.edu.unlp.pas.tp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import ar.edu.unlp.pas.tp.model.Direccion;
import lombok.Getter;
import lombok.Setter;

public class DireccionDTO {
  @JsonProperty("Calle")
  @Getter @Setter private String calle;

  @JsonProperty("Numero")
  @Getter @Setter private String numero;

  @JsonProperty("Piso")
  @Getter @Setter private String piso;

  @JsonProperty("Departamento")
  @Getter @Setter private String departamento;

  @JsonProperty("Localidad")
  @Getter @Setter private String localidad;

  @JsonProperty("Provincia")
  @Getter @Setter private String provincia;

  @JsonProperty("Pais")
  @Getter @Setter private String pais;

  public Direccion toDireccion() {
    Direccion direccion = new Direccion();
    direccion.setCalle(calle);
    direccion.setNumero(numero);
    direccion.setPiso(piso);
    direccion.setDepartamento(departamento);
    direccion.setLocalidad(localidad);
    direccion.setProvincia(provincia);
    direccion.setPais(pais);
    return direccion;
  }
}
