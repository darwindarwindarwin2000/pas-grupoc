package ar.edu.unlp.pas.tp.controllers;

import ar.edu.unlp.pas.tp.dto.DireccionDTO;
import ar.edu.unlp.pas.tp.services.DireccionService;
import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ar.edu.unlp.pas.tp.model.*;
import org.springframework.security.access.annotation.Secured;

@RestController
@RequestMapping("api/direccion")
public class DireccionController{

  @Autowired
  private DireccionService direccionService;


  //@PreAuthorize("hasRole({'ADMIN', 'USUARIO'})")
  @GetMapping("/all")
  @Secured({"ADMIN", "USUARIO"})
  public List<DireccionDTO> getAllDirecciones(){
    return direccionService.findAll();
  }

  //@PreAuthorize("hasRole({'ADMIN', 'USUARIO'})")
  @PostMapping("/create")
  @Secured({"ADMIN", "USUARIO"})
  public ResponseEntity creatDireccion(
          @RequestHeader("Authorization") String token,
          @RequestBody DireccionDTO direccionDTO
  ) {

    Direccion d = direccionDTO.toDireccion();
    Direccion savedDireccion = direccionService.save(d);
    return ResponseEntity.created(URI.create("/api/direcciones/" + savedDireccion.getId()))
      .body(savedDireccion);
  }
}
